public enum Ticket {

    SINGLE_AB("Einzelfahrschein Berlin AB",
            2.90),
    SINGLE_BC("Einzelfahrschein Berlin BC", 3.30),
    SINLGLE_ABC("Einzelfahrschein Berlin ABC", 3.60),
    SCHORT_TRIP("Kurzstrecke", 1.90),
    DAY_AB("Tageskarte Berlin AB", 8.60),
    DAY_BC("Tageskarte Berlin BC", 9.00),
    DAY_ABC("Tageskarte Berlin ABC", 9.60),
    SMALL_GROUP_DAY_AB("Kleingruppen-Tageskarte Berlin AB", 23.50),
    SMALL_GROUP_DAY_BC("Kleingruppen-Tageskarte Berlin BC", 24.30),
    SMALL_GROUP_DAY_ABC("Kleingruppen-Tageskarte Berlin ABC", 24.90);

    private final String name;
    private final double costs;

    Ticket(String name, double costs) {
        this.name = name;
        this.costs = costs;
    }

    public String getName() {
        return this.name;
    }

    public double getCosts() {
        return this.costs;
    }

}
