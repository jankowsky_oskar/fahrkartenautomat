﻿import java.util.Scanner;

class Fahrkartenautomat {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (true) {

            if (!wantToBuy(sc))
                break;

            double zuZahlenderBetrag = fahrkartenbestellungErfassen(sc);

            // Geldeinwurf
            // -----------
            double eingezahlterGesamtbetrag = 0.0;
            while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {

                eingezahlterGesamtbetrag += fahrkartenBezahlen(sc, zuZahlenderBetrag - eingezahlterGesamtbetrag);

            }

            // Fahrscheinausgabe
            // -----------------
            fahrkartenAusgeben();

            // Rückgeldberechnung und -Ausgabe
            // -------------------------------
            double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
            if (rückgabebetrag > 0.0) {
                rueckgeldAusgeben(rückgabebetrag);
            }

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                    + "Wir wünschen Ihnen eine gute Fahrt.");

        }
        sc.close();
    }

    static boolean wantToBuy(Scanner sc) {
        System.out.println("Möchten Sie Fahrkarten kaufen? (j/n)");
        String answer = sc.nextLine();
        return answer.equals("j");
    }

    static double fahrkartenbestellungErfassen(Scanner sc) {

        double zuZahlenderBetrag = 0;

        System.out.println("[0] exit");

        for (int i = 0; i < Ticket.values().length; i++) {
            Ticket ticket = Ticket.values()[i];
            System.out.printf("[%d] %15.2f %10s\n", i + 1, ticket.getCosts(), ticket.getName());
        }

        while (true) {
            System.out.print("Welches Ticket soll gekauft werden?: ");
            int selection = sc.nextInt();

            if (selection == 0)
                break;

            Ticket ticket = Ticket.values()[selection - 1];
            zuZahlenderBetrag += ticket.getCosts();
            System.out.println("Aktuelle Kosten: " + zuZahlenderBetrag);
        }

        return zuZahlenderBetrag;
    }

    static void rueckgeldAusgeben(double rückgabebetrag) {
        System.out.println("Der Rückgabebetrag in Höhe von");
        muenzeAusgeben(rückgabebetrag, "eur");
        System.out.println("wird in folgenden Münzen ausgezahlt:");

        while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
            muenzeAusgeben(2, "eur");
            rückgabebetrag -= 2.0;
        }
        while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
            muenzeAusgeben(1, "eur");
            rückgabebetrag -= 1.0;
        }
        while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
            muenzeAusgeben(50, "ct");
            rückgabebetrag -= 0.5;
        }
        while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
            muenzeAusgeben(20, "ct");
            ;
            rückgabebetrag -= 0.2;
        }
        while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
            muenzeAusgeben(10, "ct");
            rückgabebetrag -= 0.1;
        }
        while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
            muenzeAusgeben(5, "ct");
            rückgabebetrag -= 0.05;
        }
    }

    static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n\n");
    }

    static double fahrkartenBezahlen(Scanner sc, double zuZahlen) {
        System.out.println("Noch zu zahlen: " + String.format("%.2f Euro", zuZahlen));
        System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
        return sc.nextDouble();
    }

    static void warte(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static void muenzeAusgeben(double betrag, String einheit) {
        System.out.print(betrag + einheit);
    }
}
/*
 * 1. Variablen: double zuZahlenderBetrag double eingezahlterGesamtbetrag double
 * eingeworfeneMünze double rückgabebetrag
 * 
 * 2. Operationen zuZahlenderBetrag - eingezahlterGesamtbetrag
 * eingezahlterGesamtbetrag += eingeworfeneMünze eingezahlterGesamtbetrag -
 * zuZahlenderBetrag; rückgabebetrag -= BETRAG
 * 
 * 5. Ich hab int gewählt weil die Anzahl der Tickets nur eine ganze Zahl sein
 * kann. 6. Man rechnet den Preis eines Tickets mal der Anzahl.
 * 
 */